package com.tatahealth.ReusableModules;

import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class Web_Testbase {
	
	//public static WebDriver driver;
	public static int rsc = 1;


	public static final String USERNAME = "venkatpalle";
	public static final String ACCESS_KEY = "4642e843-f600-4e6b-848d-00011ec5a086";


	public static final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";
	
	//public static String input = System.getProperty("env").toUpperCase();
	public static String input = "STAGING";
	
	
	public static WebDriver start(String Name, WebDriver driver) throws Exception{
		
		if(SheetsAPI.getDataConfig(input+".runInSauceLabs").equalsIgnoreCase("TRUE")){
			System.out.println("entered here");
			DesiredCapabilities caps = DesiredCapabilities.chrome();
			
			ChromeOptions options = new ChromeOptions();
			//options.addArguments("headless");
			//options.addArguments("window-size=1366,728");
			options.addArguments("--disable-notifications");
			caps.setCapability("screenResolution", "1920x1080");
			caps.setCapability(ChromeOptions.CAPABILITY, options);
			caps.setCapability("screenResolution", "1920x1080");
			caps.setCapability("platform", "Windows 10");
			caps.setCapability("version", "85");
			caps.setCapability("name", Name);
			caps.setCapability("recordScreenshots", "false");
			caps.setCapability("timeZone", "kolkata");
			caps.setCapability("maxDuration", 10800);
			driver = new RemoteWebDriver(new URL(URL), caps);
			
		}else {
			
			ChromeOptions options = new ChromeOptions();
			//options.addArguments("headless");
			options.addArguments("window-size=1920,1080");
			options.addArguments("--disable-notifications");
			 options.addArguments("--disable-extensions");
			options.addArguments("-no-sandbox");
//			options.addArguments("--window-size=1920,1080");
			options.addArguments("--disable-dev-shm-usage");
			if(System.getProperty("os.name").toLowerCase().startsWith("win")){
				System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
			}else if(System.getProperty("os.name").toLowerCase().startsWith("lin")){
				System.setProperty("webdriver.chrome.driver", "./chromedriver");
			}
			
			driver =new ChromeDriver(options);
			driver.manage().window().maximize();
			
		}
		
		Web_GeneralFunctions.wait(8);
		return driver;
	}
	
	
	/*
	 * public void close(){ driver.close(); }
	 */

}
