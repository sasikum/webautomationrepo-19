package com.tatahealth.API.Billing;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.tatahealth.API.Billing.PurchasePackage;
import com.tatahealth.API.Core.CommonFunctions;

public class AppointmentSlotSelection extends Login {

	// private String searchString = "Report Automation";
	// private String searchString = "Banglore Doc";
	// private String searchString = "Report Doctor Three";
	// private String searchString = "shinchan";

	public void searchDoctor() throws Exception {

		url = baseurl + "/AppointmentService/api/v1/searchDoctorsAndClinicsByString";

		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("userId", consumerUserId));
		headers.add(new BasicNameValuePair("tata_auth_token", consumerToken));

		JSONObject param = new JSONObject();
		param.put("searchString", doctorSearchString);
		param.put("consumerLatitude", consumerLatitude);
		param.put("consumerLongitude", consumerLongitude);

		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);

		JSONArray arr = responseObj.getJSONObject("responseData").getJSONObject("DoctorsAndClinics")
				.getJSONArray("listOfcclinicUsers");

		JSONObject obj = (JSONObject) arr.get(0);

		slugName = obj.getString("userSlugName");
		Integer id = obj.getInt("organizationId");
		orgId = id.toString();
		System.out.println("orgID" + orgId);
		id = obj.getInt("doctorId");
		doctorId = id.toString();


	}

	public void getDoctorInfo() throws Exception {

		url = baseurl + "/AppointmentService/api/v1/getDoctorInfo/" + doctorId + "/" + consumerUserId + "/" + orgId
				+ "/" + consumerLatitude + "/" + consumerLongitude + "/";
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("userId", consumerUserId));
		headers.add(new BasicNameValuePair("tata_auth_token", consumerToken));

		// System.out.println("headers : "+headers + " url : "+url);

		JSONObject responseObj = func.getRequestWithHeaders(url, headers);
		JSONArray arr = responseObj.getJSONObject("responseData").getJSONObject("rewardInfo").getJSONObject("modeBean")
				.getJSONArray("modeBean");

		// System.out.println("get info response : "+responseObj);
		JSONObject obj = null;
		for (int i = 0; i < arr.length(); i++) {
			obj = (JSONObject) arr.get(i);
			if (obj.getString("modeName").equalsIgnoreCase("In-Person")) {

				// get appointment price
				appointmentType = obj.getString("modeName");
				appointmentAmount = obj.getInt("actualPrice");
			}
		}

		dateSlotId = responseObj.getJSONObject("responseData").getJSONObject("availableTimeSlots").getInt("dateSlotId");
		Long dateOfAppt = responseObj.getJSONObject("responseData").getJSONObject("availableTimeSlots")
				.getLong("selectedDate");
		 appointmentBookedDate =dateOfAppt;
		appointmentDate = millSecondMyFormat(dateOfAppt);
		// get reward balance amount of consumer
		rewardBalance = responseObj.getJSONObject("responseData").getJSONObject("rewardInfo")
				.getJSONObject("RewardBalance").getInt("rewardAmount");

		JSONObject inpersonSlotObj = responseObj.getJSONObject("responseData").getJSONObject("availableTimeSlots")
				.getJSONObject("inPersonConsultationSlots");
		JSONArray arrSlotObj = null;
		// System.out.println(inpersonSlotObj.length());
		String timings = null;
		JSONObject slot = null;

		for (int i = 0; i < 3; i++) {
			boolean slotStatus=true;
			if (i == 0) {
				timings = "availableSlotsInMoring";
			} else if (i == 1) {
				timings = "availableSlotsInAfterNoon";
			} else if (i == 2) {
				timings = "availableSlotsInEvening";
			}
			arrSlotObj = inpersonSlotObj.getJSONArray(timings);
			if(arrSlotObj.length()<1)
			{
				continue;
			}
			for (int j = 0; j < arrSlotObj.length(); j++) {
				slot = arrSlotObj.getJSONObject(j);
					bookedSlot = slot;
					Integer getSlotId = slot.getInt("clinicUserSlotId");
					slotStatus = checkSelectedSlotisAvailable();
					if (slotStatus == true) {
						continue;
					}
					else
					{
						break;
					}
			}
           
		if (slotStatus == true) {
			System.out.println(" slot is not available, Checking with other Timings");
			getDoctorInfo();
		}
		else
		{
			break;
		}
		}
	}

	// Eg: "Package", "Full Payment" or "Coupons", "Partial Payment" etc.,
	public void packageAndRewardDetails(String modeOfPayment, String typeOfPayment) throws Exception {
		url = baseurl + "/AppointmentService/api/v1/packages-rewards-details/" + doctorId + "/" + consumerUserId + "/"
				+ orgId + "/" + appointmentType + "/";

		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("userId", consumerUserId));
		headers.add(new BasicNameValuePair("tata_auth_token", consumerToken));

		JSONObject responseObj = func.getRequestWithHeaders(url, headers);
		JSONArray arr = null;
		// System.out.println("mode of : "+modeOfPayment);
		if (modeOfPayment.equalsIgnoreCase("Coupon")) {
			arr = responseObj.getJSONObject("responseData").getJSONObject("coupons").getJSONArray("validCoupons");
		} else if (modeOfPayment.equalsIgnoreCase("Package")) {
			arr = responseObj.getJSONObject("responseData").getJSONArray("packageInfo");
			// Check if the package you are expecting is available for the user
			JSONObject temp;
			boolean packageavailable = false;
			for (int i = 0; i < arr.length(); i++) {
				temp = arr.getJSONObject(i);
				String packagename = temp.getString("packageName");
				if (packagename.equalsIgnoreCase(typeOfPayment)) {
					packageavailable = true;
					break;
				}
			}

			if (!packageavailable) {
				System.out.println("Package Array is empty and buying");
				// Buy packages if packageInfo array is empty.
				PurchasePackage packagePurchaseObj = new PurchasePackage();
				boolean foundpackageyn = packagePurchaseObj.getMyPurchasePackage(typeOfPayment);
				if (foundpackageyn) {
					boolean purchaseStatus = packagePurchaseObj.purchasePackage();
					// Once purchased, check the for the availability of package again
					if (purchaseStatus) {
						packageAndRewardDetails(modeOfPayment, typeOfPayment);
					}
				} else {
					throw new Exception("Package Purchase failed!!!");
				}

			}
		} else if (modeOfPayment.equalsIgnoreCase("Rewards")) {
			rewardBalance = responseObj.getJSONObject("responseData").getJSONObject("rewardInfo")
					.getJSONObject("RewardBalance").getInt("rewardAmount");
			// System.out.println("reward balace : "+rewardBalance);
			boolean status = checkRewardBalance(typeOfPayment);
			if (status == true) {
				packageAndRewardDetails(modeOfPayment, typeOfPayment);
			}
		}

		if (modeOfPayment.equalsIgnoreCase("Coupon") || modeOfPayment.equalsIgnoreCase("Package")) {
			JSONObject tempObj = null;
			for (int i = 0; i < arr.length(); i++) {
				tempObj = arr.getJSONObject(i);
				if (modeOfPayment.equalsIgnoreCase("Coupon")) {
					couponName = tempObj.getString("couponCode");
					if (couponName.equalsIgnoreCase(typeOfPayment)) {
						couponpackageObj = tempObj;
						break;
					}
				} else if (modeOfPayment.equalsIgnoreCase("Package")) {
					System.out.println("Found out sir ji...");
					packageName = tempObj.getString("packageName");
					if (packageName.equalsIgnoreCase(typeOfPayment)) {
						carePackageId = tempObj.getInt("carePackagesId");
						purchasePackageId = tempObj.getInt("purchasePckagesId");
						couponpackageObj = tempObj;
						break;
					}
				}
			}
		}

		// System.out.println("done with package and reward method :
		// "+couponpackageObj);

	}

	public boolean checkSelectedSlotisAvailable() throws Exception {
		boolean slotStatus = false;
		url = baseurl + "/HPPClinicsAppointmentsRestApp/rest/clinicAppointments/3.0/checkSelectedAppointmentIsAvailable";
		System.out.println("Selected slot is:"+bookedSlot.getInt("clinicUserSlotId"));
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("userId", consumerUserId));
		headers.add(new BasicNameValuePair("tata_auth_token", consumerToken));
		
		JSONObject param = new JSONObject();
		param.put("appointmentType", appointmentType);
		param.put("clinicUserSlotId", bookedSlot.getInt("clinicUserSlotId"));
		param.put("consumerId", consumerUserId);
		param.put("doctorId", doctorId);
		param.put("organizationId", orgId);
		param.put("requestConsumerId", consumerUserId);
		
		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		message = responseObj.getJSONObject("status").getString("message");
		if(!message.equals("Selected slots is available")) {
			slotStatus = true;
			
		}
		
		return slotStatus;
	}

	public void redemptionCoupon() throws Exception {

		url = baseurl + "/HPPClinicsAppointmentsRestApp/rest/clinicAppointments/3.0/redeem-coupon";

		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("userId", consumerUserId));
		headers.add(new BasicNameValuePair("tata_auth_token", consumerToken));
		// headers.add(new BasicNameValuePair("userId",consumerUserId));

		JSONObject param = new JSONObject();
		param.put("orgId", orgId);
		param.put("userId", consumerUserId);
		param.put("couponId", couponpackageObj.getString("couponId"));
		param.put("discountAmount", couponpackageObj.getInt("discountAmount"));

		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		redemptionId = responseObj.getJSONObject("responseData").getString("redemptionId");
	}

	public Integer getPackageDiscount() throws Exception {
		Integer disAmount = 0;
		JSONArray arr = couponpackageObj.getJSONArray("modeBean");
		JSONObject modeObj = null;
		for (int i = 0; i < arr.length(); i++) {
			modeObj = arr.getJSONObject(i);
			if (modeObj.getString("modeName").equalsIgnoreCase("In-Person")) {
				disAmount = modeObj.getInt("offerPrice");
			}
		}
		return disAmount;
	}

	public boolean checkRewardBalance(String modeOfPayment) throws Exception {
		boolean status = false;
		int requiredFitCoins;
		// System.out.println("check reward balance");
		if (modeOfPayment.equalsIgnoreCase("Full")) {
			if (rewardBalance < appointmentAmount) {
				FitCoins coins = new FitCoins();
				// coins.getMedicalDetailsOfConsumer();
				// coins.updateMedicalDetails();
				requiredFitCoins = appointmentAmount - rewardBalance;
				coins.getFitCoins(requiredFitCoins);
				status = true;
			}
		} else if (modeOfPayment.equalsIgnoreCase("Partial")) {
			FitCoins coins = new FitCoins();
			if (rewardBalance == 0) {
				// coins.getMedicalDetailsOfConsumer();
				// coins.updateMedicalDetails();
				requiredFitCoins = 50;
				coins.getFitCoins(requiredFitCoins);
				status = true;
			}

		}
		Thread.sleep(10000);
		return status;
	}

}
