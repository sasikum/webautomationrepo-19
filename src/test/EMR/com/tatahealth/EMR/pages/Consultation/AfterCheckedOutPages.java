package com.tatahealth.EMR.pages.Consultation;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class AfterCheckedOutPages {

	
	public WebElement getTimeLine(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='myCarouselpatientvisit']//div[@class='carousel-inner']/div[1]/div[1]/div[1]/a";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getTimeLinePrintAll(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[contains(text(),'Print All')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getTimeLinePrintSummaryPDF(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[contains(text(),'Print Summary PDF')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getTimeLineData(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='summaryContent']/div/table/tbody/tr[4]/td[2]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getCloseTimeLineModal(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='patienttimelineModal']//span[contains(text(),'×')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public boolean getReferralHeader(WebDriver driver,ExtentTest logger)throws Exception{
		
		//String xpath = "//input[@id='referralId']";
		String xpath="//div[@class='referawwarp']/div[@class='container']";
		//WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		boolean status = Web_GeneralFunctions.isDisplayed(xpath, driver);
		return status;
	}
	
	
	public WebElement getReferralDivElements(int divNo,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@class='referawwarp']/div/div["+divNo+"]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getReferralDivElementHeaders(int divNo,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@class='referawwarp']/div/div["+divNo+"]/strong";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getReferralDivElementValues(int divNo,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@class='referawwarp']/div/div["+divNo+"]/div";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getDismissElement(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='warningdismis']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getTimeLineLeftSideArrowMark(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@class='patientvisithistory']/div/div/a[1]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement moveToStartPointOfTimeLine(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@class='patientvisithistory']/div/div/div/div[1]/div/div/a";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		/*
		
		String xpath = "//div[@class='patientvisithistory']/div/div/div/div";
		List<WebElement> li = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		int size = li.size();
		System.out.println("timeline size :"+size);
		
		int i=0;
		while(i<li.size()-1) {
			Web_GeneralFunctions.click(getTimeLineLeftSideArrowMark(driver, logger), "Click arrow mark", driver, logger);
			Thread.sleep(1000);
			i++;
		}
		
		xpath = "//div[@class='patientvisithistory']/div/div/div/div[1]/div/div/a";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;*/
	}
	
	
}
