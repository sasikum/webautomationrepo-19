package com.tatahealth.EMR.Scripts.AppointmentDashboard;

import java.util.ArrayList;
import java.util.Date;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Digitizer;
import com.tatahealth.EMR.pages.AppointmentDashboard.AllSlotsPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.AppointmentsPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.ConsultationPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.PreConsultationPage;
import com.tatahealth.EMR.pages.CommonPages.CommonPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class Appointment_Digitizer {
	
	ExtentTest logger;
	
	
	@BeforeClass(groups= {"Regression","Appointment_Digitizer"})
	public static void beforeClass() throws Exception {
		Login_Digitizer.LoginTest();
		System.out.println("Initialized Driver");
	}
	
	@AfterClass(groups= {"Regression","Appointment_Digitizer"})
	public static void afterClass() throws Exception {
		Login_Digitizer.LogoutTest();
		System.out.println("Closed Driver");
	}
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * Check Whether Date is defaulted to today in All Slots Page
	 */
	@Test(priority=51,groups= {"Regression","Appointment_Digitizer"})
	public synchronized void APP_DIGI_001() throws Exception {
		Thread.sleep(3000);
		logger = Reports.extent.createTest("APP_DIGI_001");
		AppointmentsPage appoiPage = new AppointmentsPage();
		String EMRDate = appoiPage.getCurrentDate(Login_Digitizer.driver, logger).getText();
		Date date = new Date();
		String localDate = date.toString().substring(8, 10);
		Assert.assertEquals(localDate, EMRDate, "Date is not Correct. Please Check");
		
	}
	
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * Booking Appointment - Reschedule + Reschedule rescheduled appointment + cancel Appointment
	 */
	
	@Test(priority=53,groups= {"Regression","Appointment_Digitizer"})
	public synchronized void APP_DIGI_003() throws Exception {
		
		logger = Reports.extent.createTest("APP_DIGI_003");
		
		
		
		AllSlotsPage ASpage = new AllSlotsPage();
		
		//Getting Available Slot Code
		int n = ASpage.getAvailableSlot(Login_Digitizer.driver, logger);
		System.out.println(n);
		String slotId = ASpage.getSlotId(n, Login_Digitizer.driver, logger);
		String prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		
		//Scrolling to available slot and booking appointment
		ScrollAndBookAllSlots(prevSlotId, slotId);
		
		
		//Rescheduling appointment to next slot
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(n, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(6000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n, 3, Login_Digitizer.driver, logger),"Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		//generalFunctions.click(ASpage.getReschedulefromDropdown(n, Login_Digitizer.driver, logger),"Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getTimingsfromRescheduleAlert(Login_Digitizer.driver, logger), "Clicking to get timings in Reschedule Popup", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.setTimingsinRescheduleAlert((n+2), Login_Digitizer.driver, logger), "Clicking to select next slot timings in Reschedule Popup", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getOKBtnfromRescheduleAlert(Login_Digitizer.driver, logger), "Clicking on Ok in Popup", Login_Digitizer.driver, logger);
		Thread.sleep(10000);
		
		
		//Rescheduling the Rescheduled Appointment
		n=n+1;
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(n, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(6000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n, 3, Login_Digitizer.driver, logger),"Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		//generalFunctions.click(ASpage.getReschedulefromDropdown(n, Login_Digitizer.driver, logger),"Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getTimingsfromRescheduleAlert(Login_Digitizer.driver, logger), "Clicking to get timings in Reschedule Popup", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.setTimingsinRescheduleAlert((n+2), Login_Digitizer.driver, logger), "Clicking to select next slot timings in Reschedule Popup", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getOKBtnfromRescheduleAlert(Login_Digitizer.driver, logger), "Clicking on Ok in Popup", Login_Digitizer.driver, logger);
		Thread.sleep(10000);
		
		
		//Canceling the Rescheduled Appointment
		n=n+1;
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(n, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,4, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getReasondropdownfromCancelAlert(Login_Digitizer.driver, logger), "Clicking to get reasons in Cancel Alert", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.setReasoninCancelAlert(Login_Digitizer.driver, logger),"Clicking on reasons in Cancel Alert", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getSubmitBtnfromCancelAlert(Login_Digitizer.driver, logger), "Clicking on Submit in Alert", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * BA + CI + Pre consulting + preconsulted - Consultation
	 */
	@Test(priority=54,groups= {"Regression","Appointment_Digitizer"})
	public synchronized void APP_DIGI_004() throws Exception {
		
		logger = Reports.extent.createTest("APP_DIGI_004");
		
		
		
		AllSlotsPage ASpage = new AllSlotsPage();
		CommonPage CPage = new CommonPage();
		PreConsultationPage PreconPage = new PreConsultationPage();
		
		//Getting Available Slot Code
		int n = ASpage.getAvailableSlot(Login_Digitizer.driver, logger);
		String slotId = ASpage.getSlotId(n, Login_Digitizer.driver, logger);
		String prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		
		//Scrolling to available slot and booking appointment
		ScrollAndBookAllSlots(prevSlotId, slotId);
		
		//Checking in booked appointment
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Digitizer.driver, logger), "Clicking to get Appointment Dropdown", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
		//Check-in to Preconsulting
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Digitizer.driver, logger), "Clicking on Dropdown in Checked in  Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
		//Preconsulting to Appointment Dashboard
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Thread.sleep(5000);
		
		//Preconsulting to Preconsulted
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
		//preconsulted to All Slots Page
		Web_GeneralFunctions.scrollToElement(PreconPage.getSaveBtn(Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(PreconPage.getSaveBtn(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
		//Preconsulted to Consult
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		ConsultPopupCheck();
		Thread.sleep(6000);
		
		//Moving to Appointments Page
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		
		//Moving to All slots Page
		Thread.sleep(6000);
	}
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * BA + Consult1 + Consult2 + Checkout - View and Print
	 */
	@Test(priority=55,groups= {"Regression","Appointment_Digitizer"})
	public synchronized void APP_DIGI_005() throws Exception {
		
		logger = Reports.extent.createTest("APP_DIGI_005");
		
		
		
		AllSlotsPage ASpage = new AllSlotsPage();
		CommonPage CPage = new CommonPage();
		ConsultationPage ConsPage = new ConsultationPage();
		
		//Getting Available Slot Code
		int n = ASpage.getAvailableSlot(Login_Digitizer.driver, logger);
		String slotId = ASpage.getSlotId(n, Login_Digitizer.driver, logger);
		String prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		
		//Scrolling to available slot and booking appointment
		ScrollAndBookAllSlots(prevSlotId, slotId);
		
		//Consulting with booked appointment
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,2, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		
		ConsultPopupCheck();
		
		Thread.sleep(6000);
		
		//Consult to Appointments
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		
		//Appointments to All slots page
		Thread.sleep(6000);
		
		//To Consult Again
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
		//Scroll and Checkout
		Web_GeneralFunctions.scrollToElement(ConsPage.getCheckoutBtn(Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ConsPage.getCheckoutBtn(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		NoDiagnosisCheck();
		Web_GeneralFunctions.click(ConsPage.getCloseBtninPopup(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
		//Billing page to Appointments
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		
		//Appointments to All slots page
		Thread.sleep(6000);
		
		//To View Again
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
		//Scroll and Print
		Web_GeneralFunctions.scrollToElement(ConsPage.getPrintAllBtn(Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ConsPage.getPrintAllBtn(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Thread.sleep(3000);
		
		//Closing the printed page
		ArrayList<String> tabs = new ArrayList<String> (Login_Digitizer.driver.getWindowHandles());
		Login_Digitizer.driver.switchTo().window(tabs.get(1));
		Login_Digitizer.driver.close();
		tabs = new ArrayList<String> (Login_Digitizer.driver.getWindowHandles());
		Login_Digitizer.driver.switchTo().window(tabs.get(0));
		
		
		//Moving to Appointments Page from Checked out consultation
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public synchronized void ScrollAndBookAllSlots(String prevSlotId,String slotId) throws Exception {
		
		
		
		AllSlotsPage ASpage = new AllSlotsPage();
		Thread.sleep(3000);
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getPlusIconforAvailableSlot(slotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getPlusIconforAvailableSlot(slotId, Login_Digitizer.driver, logger), "Clicking on Plus Icon", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.getSearchBoxinAvailableSlot(slotId, Login_Digitizer.driver, logger), "8553406065", "sending value in search box", Login_Digitizer.driver, logger);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getFirstAvailableConsumer(Login_Digitizer.driver, logger), "Clicking on first available consumer", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.setReasonforVisit(Login_Digitizer.driver, logger), "EMR Automation Testing", "Setting Visit Reason", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getBookAppointmentSubmitBtn(Login_Digitizer.driver, logger), "Clicking to Submit Appointment", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
	}
	
	
	
	public synchronized void appCountCheck(int prevCnt,boolean inc) throws Exception {
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		int appCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		if(inc&&appCnt==prevCnt+1) {
			System.out.println("Count Properly Increased");
		}else if(!inc&&appCnt==prevCnt-1) {
			System.out.println("Count Properly Increased");
		}else {
			System.out.println("Error: Please Check Counts Again");
		}
		
		
	}
	
	
	
	public synchronized void CheinCountCheck(int prevCnt,boolean inc) throws Exception {
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		int appCnt = appoiPage.getCheckedinCount(Login_Digitizer.driver, logger);
		if(inc&&appCnt==prevCnt+1) {
			System.out.println("Count Properly Increased");
		}else if(!inc&&appCnt==prevCnt-1) {
			System.out.println("Count Properly Increased");
		}else {
			System.out.println("Error: Please Check Counts Again");
		}
		
		
	}
	
	
	public synchronized void CompletedCountCheck(int prevCnt,boolean inc) throws Exception {
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		int appCnt = appoiPage.getCheckedinCount(Login_Digitizer.driver, logger);
		if(inc&&appCnt==prevCnt+1) {
			System.out.println("Count Properly Increased");
		}else if(!inc&&appCnt==prevCnt-1) {
			System.out.println("Count Properly Increased");
		}else {
			System.out.println("Error: Please Check Counts Again");
		}
		
		
	}
	
	
	public synchronized void ConsultPopupCheck() throws Exception {
		
		
		CommonPage CPage = new CommonPage();
		try {
			if(CPage.getConsultationAlert(Login_Digitizer.driver, logger,10).isDisplayed()) {
				Web_GeneralFunctions.click(CPage.getConsultationAlert(Login_Digitizer.driver, logger), "Clicking to Checkout and start new Appointment", Login_Digitizer.driver, logger);
			}
		}catch (Exception e) {
			
		}
		
	}
	
	
	public synchronized void NoDiagnosisCheck() throws Exception {
		
		
		ConsultationPage ConsPage = new ConsultationPage();
		
		if(ConsPage.getYesinSweetAlert(Login_Digitizer.driver, logger).isDisplayed()) {
			Web_GeneralFunctions.click(ConsPage.getYesinSweetAlert(Login_Digitizer.driver, logger),"Clicking to Checkout and start new Appointment", Login_Digitizer.driver, logger);
		}
		
	}
	
	
	
			
}