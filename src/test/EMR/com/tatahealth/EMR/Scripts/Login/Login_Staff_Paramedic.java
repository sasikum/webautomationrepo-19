package com.tatahealth.EMR.Scripts.Login;

import org.openqa.selenium.WebDriver;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.EMR.pages.Login.LoginPage;
import com.tatahealth.EMR.pages.Login.SweetAlertPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.Web_Testbase;

public class Login_Staff_Paramedic {
	
	public static ExtentTest logger;
	public static WebDriver driver;
	
	public synchronized static void LoginTest() throws Exception {
		
		
		//Web_Testbase testbase = new Web_Testbase();
		driver = Web_Testbase.start("Reusable Driver" ,driver);
		logger = Reports.extent.createTest("Login Doctor");
		
		//String userName = SheetsAPI.getDataFromSheet(Web_Testbase.input+"!B2");
		String userName = "STGNOVstaff";
		//String password = SheetsAPI.getDataFromSheet(Web_Testbase.input+"!B3");
		String password = "Test@123";
		String URL = SheetsAPI.getDataConfig(Web_Testbase.input+".EMRURL");
		
		driver.get(URL);
		Thread.sleep(3000);
		
//		driver.findElement(By.cssSelector("#txtUsername")).sendKeys(userName);
//		driver.findElement(By.cssSelector("#txtPassword")).sendKeys(password);
		LoginPage loginPage = new LoginPage();
		Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName, "Sending username to textbox", driver, logger);
		Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password, "Sending password to textbox", driver, logger);
		Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver, logger);
		
		Thread.sleep(3000);
		SweetAlertPage swaPage = new SweetAlertPage();
		if(swaPage.getSweetAlertCheck(driver, logger)) {
			Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert", driver, logger);
		}
		
		Thread.sleep(15000);
		
	}
	
	
	public synchronized static void LogoutTest() throws Exception {
		driver.close();
		driver.quit();
	}
	
	
			
}